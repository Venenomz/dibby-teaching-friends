﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LobsterDog;
using Sirenix.OdinInspector;


[RequireComponent(typeof(Rigidbody))] // This will tell the class that it is not allowed to work without the component RIGIDBODY.
// It's also a really cheaty way of adding the component to an object.

// This is a class. Classes can be many things, most of which will be attached to a GameObject
public class TestScript : MonoBehaviour // this script inherits from MonoBehaviour. Indicated by ":" MonoBehaviour
{
    // classes can inherit from anything. Even other classes. Doing so effectively makes it a "family tree."

    // A Foldout Group is an Odin Inspector only thing. It allows for the unity inspector to fold out instead of being flat.
    [FoldoutGroup("Variables"), SerializeField]
    Rigidbody m_rb; // Rigidbody enables physics based collsion and movement

    // SerializeField keeps the object private, but enables it to be seen in the inspector. (It also enables it to be saved.)
    [FoldoutGroup("Variables"), SerializeField]
    GameObject m_cubeToSpawn = null; // This is an object in the game. ANY OBJECT.

    [FoldoutGroup("Variables"), SerializeField] // same as above.
    GameObject m_otherCube = null;

    [SerializeField] // Don't worry about this until later.
    MyFirstEnum m_enumValue;

    [SerializeField] // Don't worry about this until later too.
    ObjectData m_data;

    [SerializeField] 
    TestScript m_otherTestObject = null;

    [System.NonSerialized] // this is a Boolean. It can only every be TRUE or FALSE. Very useful. (1 byte)
    bool m_testBool = false;

    [System.NonSerialized]
    int m_testInt = 10; // this is an interger. It's just a whole number. It can NEVER be a decimal. (4 bytes)

    // NonSerialized stops this from being saved, and hides it in the inspector. Useful for performance.
    [System.NonSerialized] // a float is a number that can include a decimal. If it does, it must include "f" at the end.
    const float MOVE_SPEED = 5f; // a "const" means this type can never be changed. Ie. This can never be anything but 5.
    // Additionally, a float has a "range" of ±3.40282347E+38F. IE. 6-7 significant digits of accuracy. (4 bytes)

    [System.NonSerialized] // A double has a "range" of ±1.79769313486231570E+308. Ie. 15-16 significant digits of accuracy.
    double m_testDouble = 52.244566; // So it's more accurate than a float, but takes up double the size. (8 bytes)

    [System.NonSerialized]
    Vector3 m_spawnPos = Vector3.zero; // Vectors are angles, but they also represent coordinates inside unity.

    // This is a list, it is basically a container. Another container is an Array, which you would make by using GameObject[];
    public List<GameObject> m_spawnPoints; // this is also a public variable, meaning anything (in theory) could access it. 
    // Note, that any container in will ALWAYS begin at 0. To acces the first index of this list, you use m_spawnPoints[0];

    // A string is basically text. It could also be an item id.
    private string m_yourName = ""; // this is also a private variable, meaning nothing outside this class can access it.
    // a maximum of (22 bytes)

    // a string is an array of chars, It's basically a single character in a string. (1 byte)
    char m_testChar = 'i';

    // Using the LobsterDog Codebase:
    // Hi, so I have developed my own codebase in which you can utilize.
    // it includes a bunch of stuff from math extensions to unity specific things.
    // you get bonus points for using any of this stuff!

    // Start is called before the first frame update
    void Start()
    {
        // use start to retrieve our rigidbody

        // use start to initialize our list.
    }

    // Update is called once per frame
    void Update()
    {
        //MakeCubeMoveUsingTransform();

        //MakeCubeMoveUsingPhysics();

        //SpawnACube();

        //IterateThroughAList();

        //NonSpecificIterationThroughAList();

        //DoANullCheck();
    }

    // Using Debug.Log("");
    void DebugLogYourName()
	{
        // set m_yourName to be your name!

        // Log it.

        throw new System.Exception("DebugLogYourName Not Yet Implemented");
    }

    // Using transform and Time.deltaTime
    void MakeCubeMoveUsingTransform()
	{
        // access our transform and multiple our position by a direction, multiplied by our speed multiplied by Time.deltaTime
        // using Time.deltaTime will make it move with our framerate (sortof.) Which will make it smooth.

        throw new System.Exception("MakeCubeMoveUsingTransform Not Yet Implemented");
	}

    // Using Rigidbody
    void MakeCubeMoveUsingPhysics() // Physics is not really good for gameplay as it's kinda unpredicable. Still useful to know.
	{
        // access the rigidbody and make it move using .Move()

        throw new System.Exception("MakeCubeMoveUsingPhysics Not Yet Implemented");
    }

    // using Instantiate
    void SpawnACube()
	{
        // Spawn the cube.

        // Change the name of the cube to be your name. You can do this by caching the spawned item.

        throw new System.Exception("SpawnACube Not Yet Implemented");
    }

    // using a for loop
    void IterateThroughAList()
	{
        // print the name of each thing in the list.

        throw new System.Exception("IterateThroughAList Not Yet Implemented");
    }

    // using a foreach loop
    void NonSpecificIterationThroughAList()
	{
        throw new System.Exception("NonSpecificIterationThroughAList Not Yet Implemented");
    }
    
    // Use a while loop. Note: BE CAREFUL. While loops can cause infinite interation which will hardlock your unity.
    void UsingAWhileLoop()
	{
        throw new System.Exception("UsingAWhileLoop Not Yet Implemented");
    }

    // Null check the cube to spawn using an if statement. Then print out if it is null or not. 
    void DoANullCheck()
	{
        throw new System.Exception("DoANullCheck Not Yet Implemented");
    }

    // to call this, use StartCoroutine()
    IEnumerator UseACoroutine() // General rule of thumb is to NEVER have this be called in update. It's better to 
	{ // Make a void function and call this in there.

        throw new System.Exception("UseACoroutine Not Yet Implemented");

        // using yield return null will delay everything under it being called by 1 frame.
        yield return null;

        // This will delay everything under it by 1 in game second. 
        yield return new WaitForSeconds(1f);

        // This will delay everyting under it by 2 real life seconds.
        yield return new WaitForSecondsRealtime(2f);
	}

    // Functions can also take variables to work. This one takes a float that will be our time to reach the othercube pos.
    void UsingLerp(float t)
	{
        // Linearly interpolates our position from one location, to the other. (Linear being from 0 to 1)
        transform.position = Vector3.Lerp(transform.position, m_otherCube.transform.position, t);

        throw new System.Exception("UsingLerp Not Yet Implemented");
    }

    void UsingSlerp(float t)
	{
        // basically the same as a lerp, but interpolates in a circle instead of linearly.
        transform.position = Vector3.Slerp(transform.position, m_otherCube.transform.position, t); // Makes smoother sometimes.

        throw new System.Exception("UsingSlerp Not Yet Implemented");
    }

    void PrintDistanceFromUsToOther(Vector3 other)
	{
        // using Vector3.Distance. It returns a float.
        throw new System.Exception("PrintDistanceFromUsToOther Not Yet Implemented");
    }

    Vector2 ConvertVector3ToVector2(Vector3 input)
	{
        throw new System.Exception("ConvertVector3ToVector2 Not Yet Implemented");
        return Vector2.zero;
	}

    Vector3 ConvertVector2ToVector3(Vector2 input)
    {
        throw new System.Exception("ConvertVector2ToVector3 Not Yet Implemented");
        return Vector3.zero;
    }

    // Functions can be bascially anything. But it it's not a void, it HAS to return the same type that it is.
    int Calculator(int x, int y) // IE. you cannot return a float in an int function. It has to be an int.
	{
        return x + y;
	}

    // Additionally, functions can also be a class, and return a class. Very useful for some things.
    TestScript Test()
	{
        return this; // you can reference this individual object, using this.
	}

    // An enum is kinda like a string in a way. But you can change game states with them. 
    public enum MyFirstEnum // everything in an enum has it's own index inside of it. However, you can change it's location if you'd like.
	{
        HI, // 0
        THIS, // 1
        IS, // 2    
        AN, // 3
        ENUM, // 4
        THESE, // 5
        ARE, // 6
        EXTREMELY, // 7
        USEFUL, // 8
        KEKW = 105, // this is not at location 9. It's now at 105.
	}

    // Change our enum 
    void SwitchEnum(MyFirstEnum test)
	{
        // you can also set a value to be the value of an object passed in.
        m_enumValue = test;

        // this is a switch. Most the time, these are used to update game states / object states. 
		switch (test)
		{
			case MyFirstEnum.HI:

				break;

			case MyFirstEnum.THIS:

				break;

			case MyFirstEnum.IS:

				break;

			case MyFirstEnum.AN:

				break;

			case MyFirstEnum.ENUM:

				break;

			case MyFirstEnum.THESE:

				break;

			case MyFirstEnum.ARE:

				break;

			case MyFirstEnum.EXTREMELY:

				break;

			case MyFirstEnum.USEFUL:

				break;

			default:
				break;
		}
	}

    // Ternary Operators are very useful and pretty easy to understand.
    int TestingTernaryOperator()
	{
        // what this does is checks if our bool is true
        return m_testBool ? 1 : 2; // if it is, then return 1. If it's not, return 2.
        // in our current situation, since m_testBool is never set to true it will always return 2.
	}

    TestScript TestingTernaryPartTwo()
	{
        // what this will do is check if our other test object is null. If it isn't then return that object, 
        // otherwise return ourselves
        return m_otherTestObject ?? this;
	}

    // Create new data by using new objectdata.
    void CreateNewObjectData()
	{
        throw new System.Exception("CreateNewObjectData Not Yet Implemented");
    }

    // We are done for now. Good job. If you want to learn more stuff, then lemme know. But this is pretty basic stuff you should know for to get started.
}

[System.Serializable]
public class ObjectData // this class inherits from nothing, meaning that it cannot be used or placed on a game object
{ // they are mostly used to store information. 
    int x;
    double y;
    TestScript.MyFirstEnum value;
    GameObject myObj;

    // This is called a constructor. You can use this to create a new ObjectData object, with the params you passed in.
    public ObjectData(GameObject _myObj, int _x, double _y, TestScript.MyFirstEnum _value)
    {
        myObj = _myObj;
        x = _x;
        y = _y;
        value = _value;
    }
}
