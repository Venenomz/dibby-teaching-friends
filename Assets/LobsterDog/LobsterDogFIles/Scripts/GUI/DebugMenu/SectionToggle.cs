using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LobsterDog;
using UnityEngine.UI;
using Sirenix.OdinInspector;

namespace LobsterDog
{
    [AddComponentMenu("LobsterDog/GUI/DebugMenu/SectionToggle")]
    [DisallowMultipleComponent]
    public class SectionToggle : GUILoadedContent
    {
        #region Reference Fields
        [SerializeField] Toggle toggle = null;
        [SerializeField] Text label = null;
        GameObject section = null;
        #endregion

        #region Fields
        [SerializeField] bool toggleOnByDefault = false;
        #endregion

        #region Properties
        #endregion

        #region Methods
        public void Initialise(GameObject a_section, string a_label)
        {
            section = a_section;
            label.text = a_label;
            UI_OnToggleChanged();
            toggle.isOn = toggleOnByDefault;
        }

        public void UI_OnToggleChanged()
        {
            SetSectionOnOff(toggle.isOn);
        }

        void SetSectionOnOff(bool a_on)
        {
            section.SetActive(a_on);
        }
        #endregion
    }
}