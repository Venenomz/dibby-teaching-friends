using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LobsterDog;
using UnityEngine.UI;
using Sirenix.OdinInspector;

namespace LobsterDog
{
	[AddComponentMenu("LobsterDog/GUI/DebugMenu/SectionToggleSpawner")]
	[DisallowMultipleComponent]
	public class SectionToggleSpawner : GUIContentLoader
	{
		#region Reference Fields
		[SerializeField, FoldoutGroup("References")] Transform sectionParent = null;
		#endregion

		#region Fields
		#endregion

		#region Properties
		#endregion

		#region Methods
		protected override void Awake()
		{
			base.Awake();

			if (sectionParent)
			{
				int skipFirstX = 2;
				for (int i = skipFirstX; i < sectionParent.childCount; ++i)
				{
					Transform nextChild = sectionParent.GetChild(i);
					SectionToggle newToggle = SpawnContent(nextChild.name + "Toggle") as SectionToggle;
					newToggle.Initialise(nextChild.gameObject, nextChild.name);
				}
			}
		}
		#endregion
	}
}