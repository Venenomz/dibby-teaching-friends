using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LobsterDog;
using UnityEngine.UI;
using Sirenix.OdinInspector;

namespace LobsterDog
{
	[AddComponentMenu("LobsterDog/GUI/DebugMenu/HotkeyText")]
	[DisallowMultipleComponent]
	public class HotkeyText : GUILoadedContent
	{
		#region Reference Fields
		[SerializeField, FoldoutGroup("References")] Text label = null;
		#endregion

		#region Fields
		#endregion

		#region Properties
		#endregion

		#region Methods
		public void Initialise(string a_text)
        {
			label.text = a_text;
		}
		#endregion
	}
}