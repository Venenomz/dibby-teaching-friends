using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

namespace LobsterDog
{
	[AddComponentMenu("LobsterDog/GUI/GUITransition")]
	[DisallowMultipleComponent]
	public abstract class GUITransition : LobsterDogBehaviour
    {
        #region Reference Fields
        //[SerializeField, FoldoutGroup("References")]
        #endregion

        public abstract void SetTransitionValue(float01 a_value);
    }
}