using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

namespace LobsterDog
{
    [AddComponentMenu("LobsterDog/GUI/SlideTransition")]
    [DisallowMultipleComponent]
    [TypeInfoBox("Set anchors and pivot so that for transitioned IN pos = 0. Eg. to slide in from left, have anchor at middle left and pivot at (0, 0.5).")]
    public class SlideTransition : GUITransition
    {
        #region Reference Fields
        //[SerializeField, FoldoutGroup("References")]
        #endregion

        public enum Direction { Top, Bottom, Left, Right };
        [SerializeField] public Direction slideFrom = Direction.Left;

        public override void SetTransitionValue(float01 a_value)
        {
            Vector2 newPos = RectTransformRef.anchoredPosition;

            switch (slideFrom)
            {
                case Direction.Left:
                {
                    newPos.x = -1 * (1 - a_value) * RectTransformRef.rect.width;
                    break;
                }
                case Direction.Right:
                {
                    newPos.x = (1 - a_value) * RectTransformRef.rect.width;
                    break;
                }
                case Direction.Top:
                {
                    newPos.y = (1 - a_value) * RectTransformRef.rect.height;
                    break;
                }
                case Direction.Bottom:
                {
                    newPos.y = -1 * (1 - a_value) * RectTransformRef.rect.height;
                    break;
                }
            }

            RectTransformRef.anchoredPosition = newPos;
        }
    }
}