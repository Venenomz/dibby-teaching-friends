using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

namespace LobsterDog
{
	[AddComponentMenu("LobsterDog/GUI/ScaleTransition")]
	[DisallowMultipleComponent]
	public class ScaleTransition : GUITransition
	{
        #region Reference Fields
        //[SerializeField, FoldoutGroup("References")]
        #endregion

        public override void SetTransitionValue(float01 a_value)
        {
            RectTransformRef.localScale = Vector3.one * a_value.Value;
        }
    }
}