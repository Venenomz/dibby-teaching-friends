using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using System.Linq;

namespace LobsterDog
{
    [AddComponentMenu("LobsterDog/GUI/GUIScreen")]
    [RequireComponent(typeof(CanvasGroup))]
    [DisallowMultipleComponent]
    public class GUIScreen : LobsterDogBehaviour
    {
        #region Reference Fields
        CanvasGroup thisCanvasGroup = null;
        CanvasGroup ThisCanvasGroup
        {
            get
            {
                if (thisCanvasGroup == null)
                {
                    thisCanvasGroup = GetComponent<CanvasGroup>();
                }
                return thisCanvasGroup;
            }
        }
        #endregion

        enum StartState { None, On, Off }
        [SerializeField] StartState setAtStart = StartState.None;

        [SerializeField] float transitionInSeconds = 0;
        public float TransitionInSeconds { get { return transitionInSeconds; } }

        [SerializeField] float transitionOutSeconds = 0;
        public float TransitionOutSeconds { get { return transitionOutSeconds; } }

        float transitionSpeed = 0;

        [SerializeField] AnimationCurve transitionCurve = new AnimationCurve(new Keyframe(0, 0, 1, 1), new Keyframe(1, 1, 1, 1));

        [SerializeField] GUITransition[] transitions = new GUITransition[] { };

        float01 transitionValue = 0;

        public bool IsInteractable
        {
            get { return ThisCanvasGroup.interactable; }
        }

        public bool IsVisible
        {
            get { return transitionValue.Value > 0; }
        }

        [Button("Turn On")]
        public void TurnOn()
        {
            ToggleOnOff(true, !Application.isPlaying);
        }

        [Button("Turn Off")]
        public void TurnOff()
        {
            ToggleOnOff(false, !Application.isPlaying);
        }

        public void Toggle()
        {
            ToggleOnOff(!IsInteractable, !Application.isPlaying);
        }

        void Awake()
        {
            if (setAtStart == StartState.On)
            {
                ToggleOnOff(true, true);
            }
            else if (setAtStart == StartState.Off)
            {
                ToggleOnOff(false, true);
            }
        }

        IEnumerator Transition()
        {
            if (transitionSpeed != 0)
            {
                while ((transitionSpeed > 0 && transitionValue.Value < 1) || (transitionSpeed < 0 && transitionValue.Value > 0))
                {
                    transitionValue += transitionSpeed * Time.deltaTime;
                    UpdateTransitions(transitionValue);
                    yield return null;
                }

                if (transitionValue.Value <= 0)
                {
                    transitionValue.Value = 0;
                    ThisCanvasGroup.blocksRaycasts = false;
                    ThisCanvasGroup.interactable = false;
                    UpdateTransitions(transitionValue);
                }
                else if (transitionValue.Value >= 1)
                {
                    transitionValue.Value = 1;
                    UpdateTransitions(transitionValue);
                }
            }
        }

        public void ToggleOnOff(bool a_on, bool a_instant = false)
        {
            if (a_instant)
            {
                transitionSpeed = 0;

                ThisCanvasGroup.blocksRaycasts = a_on;
                ThisCanvasGroup.interactable = a_on;
                UpdateTransitions(a_on ? 1 : 0);
            }
            else
            {
                if (a_on)
                {
                    transitionSpeed = 1 / transitionInSeconds;
                    ThisCanvasGroup.blocksRaycasts = true;
                    ThisCanvasGroup.interactable = true;
                }
                else
                {
                    transitionSpeed = -1 / transitionOutSeconds;
                }

                StartCoroutine("Transition");
            }
        }

        void UpdateTransitions(float01 a_value)
        {
            for (int i = 0; i < transitions.Length; ++i)
            {
                transitions[i].SetTransitionValue(transitionCurve.Evaluate(a_value.Value));
            }

            if (transitions.Length == 0)
            {
                //If no transitions are listed, hide with alpha by default.
                ThisCanvasGroup.alpha = transitionCurve.Evaluate(a_value.Value);
            }
        }
    }
}