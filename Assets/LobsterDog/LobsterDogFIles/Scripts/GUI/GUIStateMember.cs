using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using UnityEngine.Events;

namespace LobsterDog
{
    [AddComponentMenu("LobsterDog/GUI/GUIStateMember")]
    [RequireComponent(typeof(GUIScreen))][DisallowMultipleComponent]
	public class GUIStateMember : MonoBehaviour
	{
        [SerializeField] GUIState state = null;
        public GUIState State { get { return state; } }


        List<GUIState> states = new List<GUIState>();

        public enum EventOn { None, Active, Inactive, All }
        [SerializeField]
        EventOn eventsOn = EventOn.None;

        bool EventsOnActive() { return eventsOn == EventOn.Active || eventsOn == EventOn.All; }
        bool EventsOnInactive() { return eventsOn == EventOn.Inactive || eventsOn == EventOn.All; }

        [SerializeField] [ShowIf("EventsOnActive")] UnityEvent onActive = null;
        [SerializeField] [ShowIf("EventsOnInactive")] UnityEvent onInactive = null;

        GUIScreen thisGUIScreen = null;
        public GUIScreen ThisGUIScreen
        {
            get
            {
                if (thisGUIScreen == null)
                {
                    thisGUIScreen = GetComponent<GUIScreen>();
                }
                return thisGUIScreen;
            }
        }

        void Awake()
        {
            GUIStateMember[] GUIStateMembers = GetComponentsInChildren<GUIStateMember>();
            for (int i = 0; i < GUIStateMembers.Length; ++i)
            {
                states.Add(GUIStateMembers[i].state);
            }
        }

        //Called from the manager so that it's done at the right time.
        public void RegisterWithManager()
        {
            for (int i = 0; i < states.Count; ++i)
            {
                GUIStateManager.Instance.RegisterState(states[i]);
            }
        }

        void OnEnable()
        {
            for (int i = 0; i < states.Count; ++i)
            {
                states[i].Register(this);
            }
        }

        void OnDisable()
        {
            for (int i = 0; i < states.Count; ++i)
            {
                states[i].Unregister(this);
            }
        }

        /// <summary>
        /// Activates only this GUI Screen. This should only be called by the GUIStateManager when the state is changed.
        /// </summary>
        /// <param name="a_active"></param>
        public void SetActive(bool a_active)
        {
            if (a_active)
            {
                if (EventsOnActive())
                {
                    onActive.Invoke();
                }
            }
            else
            {
                if (EventsOnInactive())
                {
                    onInactive.Invoke();
                }
            }

            ThisGUIScreen.ToggleOnOff(a_active);
        }

        /// <summary>
        /// Activates the state members state through the GUIStateManager, so that it and all other state memebers will be turned on.
        /// </summary>
        public void ActivateState()
        {
            GUIStateManager.Instance.SetState(state);
        }
    }
}