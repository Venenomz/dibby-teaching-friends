using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

namespace LobsterDog
{
    [AddComponentMenu("LobsterDog/GUI/GUIStateManager")]
    [DisallowMultipleComponent]
    public class GUIStateManager : Singleton<GUIStateManager>
    {
        //All the GUIStates in the current scene (filled by GUIStateMembers on Awake)
        List<GUIState> allStates = new List<GUIState>();
        List<GUIState> stateHistory = new List<GUIState>();
        [SerializeField] protected GUIState currentState = null;

        public GUIState CurrentState { get { return currentState; } }

        protected virtual void Start()
        {
            GUIStateMember[] allStateMembers = FindObjectsOfType<GUIStateMember>();

            for (int i = 0; i < allStateMembers.Length; ++i)
            {
                allStateMembers[i].RegisterWithManager();
            }

            if (currentState != null)
            {
                //turn off all states at start (except the assigned current state)
                for (int i = 0; i < allStates.Count; ++i)
                {
                    if (allStates[i] != currentState)
                    {
                        allStates[i].SetActive(false);
                    }
                }

                SetStateDirect(currentState);
            }
        }

        /// <summary>
        /// Changes the state, activating all GUIStateMembers for the new state and deactivating all for the old one. 
        /// </summary>
        /// <param name="a_newState">The new state</param>
        public void SetState(GUIState a_newState)
        {
            if (currentState != a_newState)
            {
                SetStateDirect(a_newState);
            }
        }

        /// <summary>
        /// Sets the state (even if it's the current one)
        /// </summary>
        /// <param name="a_newState">New state</param>
        void SetStateDirect(GUIState a_newState)
        {
            if (stateHistory.Count > 0)
            {
                stateHistory[stateHistory.Count - 1].SetActive(false);
            }

            currentState = a_newState;
            stateHistory.Add(a_newState);
            a_newState.SetActive(true);
        }
        
        /// <summary>
        /// GUIStateMembers register their states here so the StateManager is aware of all GUIStates being used in the scene.
        /// </summary>
        /// <param name="a_newState"></param>
        public void RegisterState(GUIState a_newState)
        {
            if (!allStates.Contains(a_newState))
            {
                allStates.Add(a_newState);
            }
        }

        /// <summary>
        /// Activate the previous GUI State and remove the current one from the state history. Will do nothing if the state history is empty.
        /// </summary>
        public void Back()
        {
            if (stateHistory.Count > 1)
            {
                stateHistory[stateHistory.Count - 1].SetActive(false);
                stateHistory.RemoveAt(stateHistory.Count - 1);
                stateHistory[stateHistory.Count - 1].SetActive(true);
                currentState = stateHistory[stateHistory.Count - 1];
            }
        }
    }
}