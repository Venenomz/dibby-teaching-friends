//Notes: This is intended to be used in conjunction with a GUILoaddedContent, and both should be overwritten to allow for custom initialisation.
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

namespace LobsterDog
{
    [AddComponentMenu("LobsterDog/GUI/GUIContentLoader")]
    [DisallowMultipleComponent]
	public class GUIContentLoader : MonoBehaviour
	{
		#region Reference Fields
        [SerializeField, FoldoutGroup("References")] protected RectTransform contentParent;
        [SerializeField, FoldoutGroup("References")]  protected GameObject contentTemplate;
        #endregion

        protected List<GUILoadedContent> currentContent = new List<GUILoadedContent>();

        public int ContentCount { get { return currentContent.Count; } }
        
        protected virtual void Awake()
        {
            if (contentTemplate.scene.name != null) //The template is a reference within the scene rather than a link to a prefab outside the scene.
            {
                contentTemplate.SetActive(false);
            }
        }

        protected virtual GUILoadedContent SpawnContent(string a_name = "")
        {
            GameObject newGUI = Instantiate(contentTemplate, contentParent);
            newGUI.SetActive(true);

            if (a_name != "")
            {
                newGUI.name = a_name;
            }

            GUILoadedContent contentScript = newGUI.GetComponent<GUILoadedContent>();

            if (contentScript == null)
            {
                DebugTools.Log("", "No GUILoadedContent on " + newGUI.name + ". Spawned by " + name + ".");
                return null;
            }

            currentContent.Add(contentScript);
            return contentScript;
        }

        protected virtual void RemoveAllContent()
        {
            for (int i = 0; i < currentContent.Count; ++i)
            {
                currentContent[i].Remove();
            }

            currentContent.Clear();
        }

        protected virtual void RemoveContent(GUILoadedContent a_contentToRemove)
        {
            if (currentContent.Contains(a_contentToRemove))
            {
                currentContent.Remove(a_contentToRemove);
                a_contentToRemove.Remove();
            }
        }

        public virtual GUILoadedContent GetContent(int a_index)
        {
            return currentContent[a_index];
        }
    }
}