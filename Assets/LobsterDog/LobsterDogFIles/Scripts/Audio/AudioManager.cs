using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace LobsterDog
{
    //Audio Manager singleton. The point of contact with the audio system for all other scripts.
    [AddComponentMenu("LobsterDog/Audio/AudioManager")]
    [DisallowMultipleComponent]
	public class AudioManager : Singleton<AudioManager>
	{
        protected override void Awake()
        {
            base.Awake();

            FindAudioClips();
        }

        Dictionary<string, StoredAudio> sfx = new Dictionary<string, StoredAudio>(); //Dictionary containing all Stored Audios. This is populated at runtime from the AudioManager's children.
        
        List<string> ignoreTheseSounds = new List<string>(); //Add names of sounds to this list to ignore them completely. Usually for debug purposes.

        public float MusicVolumeMultiplier
        {
            get { return musicVolumeMultiplier; }
            set
            {
                musicVolumeMultiplier = Mathf.Clamp01(value);
                dirtyVolumes = true;
            }
        }
        [SerializeField] float musicVolumeMultiplier = 1.0f;

        public float SFXVolumeMultiplier
        {
            get { return sfxVolumeMultiplier; }
            set
            {
                sfxVolumeMultiplier = Mathf.Clamp01(value);
                dirtyVolumes = true;
            }
        }
        [SerializeField] float sfxVolumeMultiplier = 1.0f;

        bool dirtyVolumes = false;

        /// <summary>
        /// Return the current volume multiplier for a given audio type
        /// </summary>
        public float SettingsVolumeMultiplier(StoredAudio.AudioType a_audioType)
        {
            switch (a_audioType)
            {
                case StoredAudio.AudioType.MUSIC:
                    {
                        return MusicVolumeMultiplier;
                    }

                case StoredAudio.AudioType.SFX:
                    {
                        return SFXVolumeMultiplier;
                    }
            }

            return 1;
        }

        void Update()
        {
            //Update volumes of all audio if audio settings have changed.
            if (dirtyVolumes)
            {
                StoredAudio[] allAudio = GetComponentsInChildren<StoredAudio>();
                for (int i = 0; i < allAudio.Length; ++i)
                {
                    allAudio[i].UpdateVolumes();
                }
                dirtyVolumes = false;
            }
        }

        /// <summary>
        /// Populates sfx. Run on awake.
        /// </summary>
        void FindAudioClips()
        {
            if (sfx.Count == 0)
            {
                StoredAudio[] allAudio = GetComponentsInChildren<StoredAudio>();
                for (int i = 0; i < allAudio.Length; ++i)
                {
                    sfx.Add(allAudio[i].name, allAudio[i]);
                }
            }
        }

        /// <summary>
        /// Plays a global audio clip as long as sounds aren't muted and sound isn't already playing.
        /// </summary>
        /// <param name="a_name">The name of the audio clip</param>
        /// <param name="a_override">If true will start playing the clip again regardless of if it's already playing</param>
        public StoredAudio PlayAudioClip(string a_name, bool a_override = false)
        {
            if (!ignoreTheseSounds.Contains(a_name) && sfx.ContainsKey(a_name))
            {
                sfx[a_name].PlayAudio(a_override);
                return sfx[a_name];
            }

            return null;
        }

        /// <summary>
        /// Spawns an audio clip in a position in the world
        /// </summary>
        /// <param name="a_name">The name of the audio clip</param>
        /// <param name="a_position">The position in the world at which to spawn the clip</param>
        /// <param name="a_volumeMultiplier">A base multiplier for this spawned audio instance</param>
        /// <returns></returns>
        public SpawnedAudio PlayAudioClip(string a_name, Vector3 a_position, float a_volumeMultiplier = 1.0f)
        {
            if (!ignoreTheseSounds.Contains(a_name))
            {
                return sfx[a_name].PlayAudio(a_position, a_volumeMultiplier);
            }

            return null;
        }

        public void StopAudioClip(string a_name)
        {
            sfx[a_name].StopAudio();
        }

        public bool AudioClipIsPlaying(string a_name)
        {
            return sfx[a_name].IsPlaying();
        }
    }
}