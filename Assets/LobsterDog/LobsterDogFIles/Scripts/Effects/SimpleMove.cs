using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace LobsterDog
{
    [AddComponentMenu("LobsterDog/Effects/SimpleMove")]
    [DisallowMultipleComponent]
	public class SimpleMove : LobsterDogBehaviour
	{
        #region Reference Fields
        #endregion

        [SerializeField] Vector3 movementSpeed = Vector3.zero;
        public Vector3 MovementSpeed { get { return movementSpeed; } set { movementSpeed = value; } }

        [SerializeField] bool local = false;

        void Update()
        {
            Move();
        }

        protected virtual void Move()
        {
            if (local)
            {
                TransformRef.localPosition = TransformRef.localPosition + (movementSpeed * Time.deltaTime);
            }
            else
            {
                TransformRef.position = TransformRef.position + (movementSpeed * Time.deltaTime);
            }
        }
    }
}