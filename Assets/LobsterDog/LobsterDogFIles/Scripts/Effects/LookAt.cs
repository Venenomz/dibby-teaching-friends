using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace LobsterDog
{
    [AddComponentMenu("LobsterDog/Effects/LookAt")]
    [DisallowMultipleComponent]
	public class LookAt : Targeter
	{
		#region Reference Fields
        #endregion
             
        protected virtual void Update()
        {
            LookAtTarget();
        }

        protected virtual void LookAtTarget()
        {
            TransformRef.LookAt(Target);
        }
    }
}