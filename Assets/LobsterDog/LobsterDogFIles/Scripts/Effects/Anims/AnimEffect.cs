using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace LobsterDog
{
    [AddComponentMenu("LobsterDog/Effects/Anims/AnimEffect")]
    public abstract class AnimEffect : LobsterDogBehaviour
    {
        #region Reference Fields
        [SerializeField, FoldoutGroup("References")] AnimEngine animEngine = null;
        #endregion

        [SerializeField] bool useCurve = false;

        [SerializeField, ShowIf("useCurve")]
        [Tooltip("This is applied on top of the AnimEngine's curve. In almost all cases you will want one or the other to be linear. This is here in case you want to add multiple AnimEffects with different curves.")]
        AnimationCurve curve = new AnimationCurve(new Keyframe(0, 0, 1, 1), new Keyframe(1, 1, 1, 1));

        float Value
        {
            get { return useCurve ? curve.Evaluate(animEngine.Value) : animEngine.Value; }
        }

        protected virtual void Awake()
        {
            if (animEngine == null)
            {
                animEngine = GetComponent<AnimEngine>();

                if (animEngine == null)
                {
                    animEngine = gameObject.AddComponent<AnimEngine>();
                }
            }

            animEngine.Subscribe(this);
        }

        public void ValueChanged()
        {
            OnValueChanged(Value);
        }

        protected abstract void OnValueChanged(float a_newValue);
    }
}