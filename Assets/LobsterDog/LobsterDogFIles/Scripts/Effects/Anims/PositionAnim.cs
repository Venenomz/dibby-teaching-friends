using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace LobsterDog
{
    [AddComponentMenu("LobsterDog/Effects/Anims/PositionAnim")]
    [DisallowMultipleComponent]
    public class PositionAnim : Vec3Anim
    {
        #region Reference Fields
        #endregion

        [Button("Store Initial")]
        void StoreInitial()
        {
            initialValue = local ? TransformRef.localPosition : TransformRef.position;
        }

        [Button("Store Final")]
        void StoreFinal()
        {
            finalValue = local ? TransformRef.localPosition : TransformRef.position;
        }

        [Button("Set to Initial")]
        void SetToInitial()
        {
            Set(initialValue);
        }

        [Button("Set to Final")]
        void SetToFinal()
        {
            Set(finalValue);
        }

        protected override void Set(Vector3 a_newValue)
        {
            if (local)
            {
                TransformRef.localPosition = a_newValue;
            }
            else
            {
                TransformRef.position = a_newValue;
            }
        }
    }
}