using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace LobsterDog
{
	public abstract class Vec3Anim : AnimEffect
    {
        [SerializeField] protected bool local = true;

        [SerializeField] protected Vector3 initialValue = Vector3.zero;
        public Vector3 InitialValue
        {
            set { initialValue = value; }
        }

        [SerializeField] protected Vector3 finalValue = Vector3.zero;
        public Vector3 FinalValue
        {
            set { finalValue = value; }
        }

        public void ChangeValues(Vector3 a_intialValue, Vector3 a_finalValue)
        {
            initialValue = a_intialValue;
            finalValue = a_finalValue;
        }

        protected override void OnValueChanged(float a_newValue)
        {
            Set(Vector3.LerpUnclamped(initialValue, finalValue, a_newValue));
        }

        protected abstract void Set(Vector3 a_newValue);
    }
}