using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace LobsterDog
{
    [AddComponentMenu("LobsterDog/Effects/Anims/RotationAnim")]
    [DisallowMultipleComponent]
	public class RotationAnim : Vec3Anim
	{
        #region Reference Fields
        #endregion
        
        [Button("Store Initial")]
        void StoreInitial()
        {
            initialValue = local ? TransformRef.localEulerAngles : TransformRef.eulerAngles;
        }

        [Button("Store Final")]
        void StoreFinal()
        {
            finalValue = local ? TransformRef.localEulerAngles : TransformRef.eulerAngles;
        }

        [Button("Set to Initial")]
        void SetToInitial()
        {
            Set(initialValue);
        }

        [Button("Set to Final")]
        void SetToFinal()
        {
            Set(finalValue);
        }

        protected override void Set(Vector3 a_newValue)
        {
            if (local)
            {
                TransformRef.localEulerAngles = a_newValue;
            }
            else
            {
                TransformRef.eulerAngles = a_newValue;
            }
        }
    }
}