using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace LobsterDog
{
	public static class ExternalFiles
	{
        public static void SaveData(string a_fileName, string a_fileExtension, object a_classToSave, string a_rootDirectory)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream saveFile = File.Create(a_rootDirectory + "/" + a_fileName + "." + a_fileExtension);

            formatter.Serialize(saveFile, a_classToSave);

            saveFile.Close();
        }

        public static void SaveData(string a_fileName, string a_fileExtension, object a_classToSave)
        {
            SaveData(a_fileName, a_fileExtension, a_classToSave, Application.persistentDataPath);
        }

        public static object LoadData(string a_fileName, string a_fileExtension, object a_classToLoad, string a_rootDirectory)
        {
            if (File.Exists(a_rootDirectory + "/" + a_fileName + "." + a_fileExtension))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                FileStream saveFile = File.Open(a_rootDirectory + "/" + a_fileName + "." + a_fileExtension, FileMode.Open);

                object loadedClass = formatter.Deserialize(saveFile);

                saveFile.Close();

                return loadedClass;
            }
            else
            {
                SaveData(a_fileName, a_fileExtension, a_classToLoad, a_rootDirectory);

                return a_classToLoad;
            }
        }

        public static object LoadData(string a_fileName, string a_fileExtension, object a_classToLoad)
        {
            return LoadData(a_fileName, a_fileExtension, a_classToLoad, Application.persistentDataPath);
        }
    }
}