using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.Events;

namespace LobsterDog
{
    [CreateAssetMenu(menuName = "Lobster Dog/Variables/IntVar")]
    public class IntVar : LobsterDogScriptable
    {
        [SerializeField] bool useRange = false;

        [SerializeField, ShowIf("useRange")]
        [Tooltip("If true, this IntVar will return a random value between the specified range each time it is accessed.")]
        bool random = false;

        [SerializeField, ShowIf("useRange")] MinMaxInt range = null;
        
        public int Max { get { return useRange ? range.Max : _value; } }
        public int Min { get { return useRange ? range.Min : _value; } }

        [SerializeField, HideIf("random")] int _value = 0;
        public int Value
        {
            get
            {
                if (useRange && random)
                {
                    Value = range.Random();
                }

                return _value;
            }
            set
            {
                _value = useRange ? range.Clamp(value) : value;
            }
        }

        public float01 Value01
        {
            get { return useRange ? range.InverseLerp(_value) : 0; }
        }

        public void SetValue(int a_newValue)
        {
            Value = a_newValue;
        }

        public void AdjustValue(int a_amount)
        {
            Value += a_amount;
        }

        public void Lerp(float a_value)
        {
            if (useRange)
            {
                Value = range.Lerp(a_value);
            }
        }

        public void OnValidate()
        {
            Value = _value;
        }
    }

    [System.Serializable]
    public class IntRef
    {
        public bool UseConstant = true;
        public int ConstantValue;
        public IntVar Variable;

        public int Value
        {
            get { return UseConstant ? ConstantValue : Variable.Value; }
        }

        public static implicit operator int(IntRef a_reference)
        {
            return a_reference.Value;
        }
    }
}