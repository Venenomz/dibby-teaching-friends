using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

namespace LobsterDog
{
    [CreateAssetMenu(menuName = "Lobster Dog/Variables/FloatVar")]
    public class FloatVar : LobsterDogScriptable
    {
        public enum Type { Regular, Random, Range, Curve }

        [SerializeField] Type floatType = Type.Regular;


        //Properties for inspector attributes
        bool IsRegular() { return floatType == Type.Regular; }
        bool IsRandom() { return floatType == Type.Random; }
        bool IsRange() { return floatType == Type.Range || floatType == Type.Random; }
        bool IsCurve() { return floatType == Type.Curve; }

        [SerializeField, ShowIf("IsRange")] MinMaxFloat range = null;
        [SerializeField, ShowIf("IsCurve")] Curve curve = null;
                
        [SerializeField, HideIf("IsRandom")] float _value = 0;
        public float Value
        {
            get
            {
                if (floatType == Type.Random)
                {
                    Value = range.Random();
                }

                if (floatType == Type.Curve)
                {
                    Value = curve.Value();
                }

                return _value;
            }
            set
            {
                _value = IsRange() ? range.Clamp(value) : value;
            }
        }

        public float01 Value01
        {
            get { return IsRange() ? range.InverseLerp(_value) : 0; }
        }

        public void SetValue(float a_newValue)
        {
            Value = a_newValue;
        }

        public void AdjustValue(float a_amount)
        {
            Value += a_amount;
        }

        public void Lerp(float a_value)
        {
            if (IsRange())
            {
                Value = range.Lerp(a_value);
            }
        }

        public void OnValidate()
        {
            Value = _value;
        }

        public void SetToMax()
        {
            if (floatType == Type.Range)
            {
                Value = range.Max;
            }
            else if (floatType == Type.Curve)
            {
                Value = curve.Max;
            }
        }

        public void SetToMin()
        {
            if (floatType == Type.Range)
            {
                Value = range.Min;
            }
            else if (floatType == Type.Curve)
            {
                Value = curve.Min;
            }
        }
    }

    [System.Serializable]
    public class FloatRef
    {
        public FloatRef(float a_value)
        {
            UseConstant = true;
            ConstantValue = a_value;
        }
        public FloatRef(FloatVar a_floatVar)
        {
            UseConstant = false;
            Variable = a_floatVar;
        }
        public FloatRef(bool a_useConstant = true)
        {
            UseConstant = true;
            ConstantValue = 0;
        }

        public bool UseConstant = true;
        public float ConstantValue; //Not a constant any more..
        public FloatVar Variable;

        public float Value
        {
            get { return UseConstant ? ConstantValue : Variable.Value; }
            set
            {
                if (UseConstant)
                {
                    ConstantValue = value;
                }
                else
                {
                    Variable.Value = value;
                }
            }
        }

        public static implicit operator float(FloatRef a_reference)
        {
            return a_reference.Value;
        }
    }
}