using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.UI;
using System;

namespace LobsterDog
{
    [AddComponentMenu("LobsterDog/Debug/OnScreenConsole")]
    [DisallowMultipleComponent]
    public class OnScreenConsole : MonoBehaviour
    {
        #region Reference Fields
        [SerializeField] Text logText = null;
        [SerializeField] ScrollRect scrollRect = null;
        #endregion

        [SerializeField] bool includeStackTrace;
        string fullConsole = "";
        bool showFullConsole = false;

        void Awake()
        {
            if (logText == null)
            {
                logText = GetComponentInChildren<Text>();
                DebugTools.Log("LogText should be set in editor to make sure it gets Awake errors");
            }

            if (scrollRect == null)
            {
                scrollRect = GetComponentInChildren<ScrollRect>();
            }
        }

        [Button("Test Console Message")]
        void TestConsoleMessage()
        {
            DebugTools.Log("Test" + Time.deltaTime, DebugTools.LogMessageColour.Blue);
        }

        [HideInInspector]
        public Text t;
        [Button("Test Console Error")]
        void TestConsoleError()
        {
            print(t.name);
        }

        void OnEnable()
        {
            Application.logMessageReceived += HandleLog;
        }

        void OnDisable()
        {
            Application.logMessageReceived -= HandleLog;
        }

        void HandleLog(string a_logString, string a_stackTrace, LogType a_type)
        {
            string messageColour = "magenta";

            if (a_type == LogType.Error || a_type == LogType.Assert || a_type == LogType.Exception)
            {
                messageColour = "red";
            }
            else if (a_type == LogType.Warning)
            {
                messageColour = "yellow";
            }
            else if (a_type == LogType.Log)
            {
                messageColour = "white";
            }

            FormatRichText(a_logString, messageColour, 30);
            
            if (includeStackTrace)
            {
                FormatRichText(a_stackTrace, "silver", 20);
            }
        }

        void FormatRichText(string a_stringInput, string a_inputColour, int a_fontSize)
        {
            if (logText != null)
            {
                fullConsole += "<color=" + a_inputColour + ">" + a_stringInput + "</color> " + Environment.NewLine;
                UpdateText();
            }
        }

        void UpdateText()
        {
            logText.text = showFullConsole ? fullConsole : DebugTools.InternalLog;
        }

        public void ShowFullConsole(bool a_showFull)
        {
            showFullConsole = a_showFull;
            UpdateText();
        }

        public void IncludeStackTrace(bool a_includeStackTrace)
        {
            includeStackTrace = a_includeStackTrace;
        }
    }
}