using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.Events;

namespace LobsterDog
{
    [AddComponentMenu("LobsterDog/Debug/KeyboardActions")]
    [DisallowMultipleComponent]
    public class KeyboardActions : MonoBehaviour
    {
        [SerializeField] KeyCode masterKey = KeyCode.None;
        [SerializeField] KeyAction[] keyActions = new KeyAction[] { };

        public string MasterKeyName { get { return masterKey.ToString(); } }

        void Update()
        {
            if (masterKey == KeyCode.None || Input.GetKey(masterKey))
            {
                for (int i = 0; i < keyActions.Length; ++i)
                {
                    keyActions[i].CheckInvoke();
                }
            }
        }

        public string[] GetKeyActionsAsText()
        {
            string[] text = new string[keyActions.Length];

            for(int i = 0; i < keyActions.Length; ++i)
            {
                text[i] = keyActions[i].KeyName + ": " + keyActions[i].Name;
            }

            return text;
        }
    }

    [System.Serializable]
    public class KeyAction
    {
        [SerializeField] string _name = "";
        [SerializeField] KeyCode key = KeyCode.None;
        [SerializeField] UnityEvent actions = null;

        public string Name { get { return _name; } }
        public string KeyName { get { return key.ToString(); } }

        public void CheckInvoke()
        {
            if (Input.GetKeyDown(key))
            {
                actions.Invoke();
            }
        }
    }
}