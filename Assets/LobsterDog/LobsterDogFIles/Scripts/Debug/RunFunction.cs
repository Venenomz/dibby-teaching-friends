using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.Events;

//Shortcut for running a function in editor
namespace LobsterDog
{
    [AddComponentMenu("LobsterDog/Debug/RunFunction")]
	public class RunFunction : MonoBehaviour
	{
        #region Reference Fields
        #endregion

        [SerializeField] UnityEvent functions = null;

        [Button("Run")]
        public void Run()
        {
            functions.Invoke();
        }
    }
}