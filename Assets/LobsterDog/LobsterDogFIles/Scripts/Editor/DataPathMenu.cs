using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace LobsterDog
{
	public class DataPathMenu : Editor
	{
        [MenuItem("Lobster Dog/Open Data Path")]
        static void OpenDataPath()
        {
            System.Diagnostics.Process.Start("explorer.exe", Application.persistentDataPath.Replace('/', '\\'));
        }
    }
}