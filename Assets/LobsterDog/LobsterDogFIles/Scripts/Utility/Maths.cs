using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace LobsterDog
{
	[DisallowMultipleComponent]
	public static class Maths
	{
        public static float Round(float a_value, int a_decimalPlaces)
        {
            float exp = Mathf.Pow(10, a_decimalPlaces);
            return Mathf.Round(a_value * exp) / exp;
        }

        public static double Round(double a_value, int a_decimalPlaces)
        {
            float exp = Mathf.Pow(10, a_decimalPlaces);
            return Mathf.Round((float)a_value * exp) / exp;
        }

        public static double Min(double a, double b)
        {
            return Mathf.Min((float)a, (float)b);
        }

        public static double Max(double a, double b)
        {
            return Mathf.Max((float)a, (float)b);
        }

        public static float GetFractionPart(float a_value)
        {
            return a_value % 1;
        }

        public static bool IsEven(int a_value)
        {
            return a_value % 2 == 0;
        }

        public static bool IsOdd(int a_value)
        {
            return !IsEven(a_value);
        }

        public static Vector3 PosOnCircle(Vector3 a_center, float a_radius, int a_totalDivisions, int a_divisionIndex)
        {
            return PosOnCircle(a_center, a_radius, new float01(0, a_totalDivisions, a_divisionIndex));
        }

        public static Vector3 PosOnCircle(Vector3 a_center, float a_radius, float01 a_point)
        {
            float ang = a_point * 360;
            Vector3 pos = a_center;

            pos.x += a_radius * Mathf.Sin(ang * Mathf.Deg2Rad);
            pos.z += a_radius * Mathf.Cos(ang * Mathf.Deg2Rad);

            return pos;
        }
    }
}