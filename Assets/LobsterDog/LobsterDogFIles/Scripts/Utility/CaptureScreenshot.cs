﻿using UnityEngine;
using System.Collections;
using Sirenix.OdinInspector;

public class CaptureScreenshot : MonoBehaviour
{
    [SerializeField] int _resWidth = 2560;
    [SerializeField] int _resHeight = 1440;

    bool _takeHiResShot = false;
    bool _takeLowResShot = false;

    [SerializeField] Camera UICamera;
    [SerializeField] Camera MainCamera;

    [SerializeField] int CaptureRate = 60;

    public static string ScreenShotName(int width, int height, int frame)
    {
        return string.Format("{0}/frames/screen_{1}x{2}_{3}.png", Application.dataPath, width, height, frame);
    }

    void Awake()
    {
        MainCamera = GetComponent<Camera>();
        //       if (_takeHiResShot||_takeLowResShot)
        //           Time.captureFramerate = CaptureRate;
    }

    void LateUpdate() 
    {
        if (_takeHiResShot) 
        {
            RenderTexture rt = new RenderTexture(_resWidth, _resHeight, 24);
            MainCamera.targetTexture = rt;
            Texture2D screenShot = new Texture2D(_resWidth, _resHeight, TextureFormat.RGB24, false);
            MainCamera.Render();
            if (UICamera != null)
            {
                UICamera.targetTexture = rt;
                UICamera.Render();
            }
            RenderTexture.active = rt;
            screenShot.ReadPixels(new Rect(0, 0, _resWidth, _resHeight), 0, 0);
            MainCamera.targetTexture = null;
            if (UICamera != null)
            {
                UICamera.targetTexture = null;
            }
            RenderTexture.active = null; 
            Destroy(rt);
            byte[] bytes = screenShot.EncodeToPNG();
	        Destroy(screenShot);
            string filename = ScreenShotName(_resWidth, _resHeight, Time.frameCount);

            System.IO.File.WriteAllBytes(filename, bytes);
            Debug.Log(string.Format("Took screenshot to: {0}", filename));
//            Application.OpenURL(filename);
            _takeHiResShot = false;
        }

		if (_takeLowResShot)
		{
			string filename = ScreenShotName(Screen.width, Screen.height, Time.frameCount);
			ScreenCapture.CaptureScreenshot(filename);
			Debug.Log(string.Format("Took screenshot to: {0}", filename));
            _takeLowResShot = false;
		}
    }

    [Button]
    public void TakeLowResShot()
    {
        _takeLowResShot = true;
    }

    [Button]
    public void TakeHiResShot()
    {
        _takeHiResShot = true;
    }
}
