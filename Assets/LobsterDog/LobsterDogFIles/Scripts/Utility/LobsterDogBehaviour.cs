using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.UI;

namespace LobsterDog
{
	public abstract class LobsterDogBehaviour : MonoBehaviour
    {
        [HideInInspector] Transform transformRef = null;
        public Transform TransformRef
        {
            get
            {
                if (!transformRef)
                {
                    transformRef = GetComponent<Transform>();
                }
                return transformRef;
            }
        }

        public RectTransform RectTransformRef
        {
            get
            {
                return TransformRef as RectTransform;
            }
        }

        [HideInInspector] Rigidbody rigidBodyRef = null;
        public Rigidbody RigidBodyRef
        {
            get
            {
                if (rigidBodyRef == null)
                {
                    rigidBodyRef = GetComponent<Rigidbody>();
                }
                return rigidBodyRef;
            }
        }

        /// <summary>
        /// Prints details about this monobehaviour to the console. Justn ame by default. Overridde PrintText to change.
        /// </summary>
        /// <param name="a_additionalText"></param>
        public void Print(string a_additionalText = "")
        {
            DebugTools.Log(a_additionalText + ": " + PrintText());
        }

        protected virtual string PrintText()
        {
            return name;
        }
    }
}