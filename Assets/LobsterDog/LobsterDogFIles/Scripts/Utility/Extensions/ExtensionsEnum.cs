﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LobsterDog
{
	public static class ExtensionsEnum
	{
		public static int ToInt<T>(this T source) where T : IConvertible//enum
		{
			if (!typeof(T).IsEnum)
				throw new ArgumentException("T must be an enumerated type");

			return (int)(IConvertible)source;
		}

		public static bool HasBits<T>(this T source, T flag) where T : IConvertible//enum
		{
			if (!typeof(T).IsEnum)
				throw new ArgumentException("T must be an enumerated type");

			int flagInt = (int)(IConvertible)flag;
			return ((int)(IConvertible)source & flagInt) == flagInt;
		}
	}
}
