﻿using UnityEngine;
using System.Collections;

namespace LobsterDog
{
	public static class ExtensionsColor
	{
		public static Color LerpTo(this Color col, Color target, float amount)
		{
			return Color.Lerp(col, target, amount);
		}

		public static Color GetHighlight(this Color col)
		{
			return col + col + col + Color.grey;
		}

		public static void Set(this Color col, float r, float g, float b)
		{
			col.r = r;
			col.g = g;
			col.b = b;
		}

		public static void Set(this Color col, float r, float g, float b, float a)
		{
			col.r = r;
			col.g = g;
			col.b = b;
			col.a = a;
		}

		public static Color SetAlpha(this Color col, float a)
		{
			col.a = a;
			return col;
		}

		public static void Set(this Color32 col, byte r, byte g, byte b)
		{
			col.r = r;
			col.g = g;
			col.b = b;
		}

		public static void Set(this Color32 col, byte r, byte g, byte b, byte a)
		{
			col.r = r;
			col.g = g;
			col.b = b;
			col.a = a;
		}

		public static Color32 SetAlpha(this Color32 col, byte a)
		{
			col.a = a;
			return col;
		}

		public static void SetAlpha(this UnityEngine.UI.Graphic graphic, float a)
		{

			Color col = graphic.color;
			col.a = a;

			graphic.color = col;
		}

		public static Color HCYWeights = new Color(0.299f, 0.587f, 0.114f, 0f);

		public static Color GetGlowColor(this Color color)
		{
			float h;
			float s;
			float v;

			Color.RGBToHSV(color, out h, out s, out v);
			s = 1f;
			v = 1f;
			color = Color.HSVToRGB(h, s, v);

			Color sat = color * ExtensionsColor.HCYWeights;
			float satVal = (sat.r + sat.g + sat.b);
			s = satVal;
			color = Color.HSVToRGB(h, s, v);
			color.a = 1f;
			return color;
		}

		public static Color HexToColor(string hex)
		{
			hex = hex.Replace("0x", "");//in case the string is formatted 0xFFFFFF
			hex = hex.Replace("#", "");//in case the string is formatted #FFFFFF
			byte a = 255;//assume fully visible unless specified in hex
			byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
			byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
			byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
			//Only use alpha if the string has enough characters
			if (hex.Length == 8)
			{
				a = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
			}
			return new Color32(r, g, b, a);
		}
	}
}