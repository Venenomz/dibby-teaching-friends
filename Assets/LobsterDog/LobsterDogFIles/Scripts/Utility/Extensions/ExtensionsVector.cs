﻿using UnityEngine;
using System.Collections;

namespace LobsterDog
{
    public static class ExtensionsVector
    {

        /// T/////////////////////////////////////////////////	
        /// Vector2
        public static Vector2 SetX(this Vector2 me, float x)
        {
            return new Vector2(x, me.y);
        }

        public static Vector2 SetY(this Vector2 me, float y)
        {
            return new Vector2(me.x, y);
        }

        public static Vector3 ToVector3(this Vector2 me)
        {
            return new Vector3(me.x, me.y, 0);
        }

        public static Vector3 ToVector3(this Vector2 me, float zVal)
        {
            return new Vector3(me.x, me.y, zVal);
        }

        public static float DistanceToSquared(this Vector2 me, Vector2 other)
        {
            return (me - other).sqrMagnitude;
        }

        public static Vector2 VectorTo(this Vector2 me, Vector2 other)
        {
            return (other - me);
        }

        public static Vector2 LerpHalfCos(this Vector2 me, Vector2 target, float progress)
        {
            return me + (target - me) * 0f.LerpHalfCos(1f, progress);
        }

        public static Vector2 MoveDistanceToward(this Vector2 me, Vector2 target, float distance)
        {
            return me + (target - me).normalized * distance;
        }


        public static float Dot(this Vector2 me, Vector2 other)
        {
            return Vector2.Dot(me, other);
        }

        public static Vector2 Lerp(this Vector2 me, Vector2 target, float progress)
        {
            return me + (target - me) * progress;
        }

        // Mult => (x*x , y*y)
        public static Vector2 Mult(this Vector2 me, Vector2 other)
        {
            return new Vector2(me.x * other.x, me.y * other.y);
        }

        public static Vector2 DampToTarget(this Vector2 me, Vector2 target, float smoothing, float dt)
        {
            float factor = 1 - Mathf.Pow(smoothing, dt * 60f);

            return me.Lerp(target, factor);
        }

        public static bool IsInRange(this Vector2 me, Vector2 other, float radius = 0.1f)
        {
            return (me - other).sqrMagnitude < radius * radius;
        }

        public static bool IsValid(this Vector2 me)
        {
            return float.IsInfinity(me.x) == false && float.IsInfinity(me.y) == false && float.IsNaN(me.x) == false && float.IsNaN(me.y) == false;
        }

        public static Vector2 Clamp(this Vector2 me, float mag)
        {
            float myMag = me.magnitude;
            if (myMag > mag)
            {
                return me * (mag / myMag);
            }

            return me;
        }

        public static Vector2 AddX(this Vector2 me, float addToX)
        {
            me.x += addToX;
            return me;
        }

        public static Vector2 AddY(this Vector2 me, float addToY)
        {
            me.y += addToY;
            return me;
        }

        public static Vector2 NearestPointOnLine(this Vector2 pnt, Vector2 linePnt, Vector2 lineDirNormalised)
        {
            //lineDir.Normalize();//this needs to be a unit vector

            var v = pnt - linePnt;
            var d = Vector3.Dot(v, lineDirNormalised);
            return linePnt + lineDirNormalised * d;
        }

        public static Vector2 Clamp(this Vector2 me, Rect area)
        {
            me.x = me.x.Clamp(area.xMin, area.xMax);
            me.y = me.y.Clamp(area.yMin, area.yMax);

            return me;
        }

        public static Vector2 PerpendicularClockwise(this Vector2 vector2)
        {
            return new Vector2(-vector2.y, vector2.x);
        }

        public static Vector2 PerpendicularCounterClockwise(this Vector2 vector2)
        {
            return new Vector2(vector2.y, -vector2.x);
        }

        /// T/////////////////////////////////////////////////	
        /// Vector3


        public static Vector2 ToVector2(this Vector3 me)
        {
            return new Vector2(me.x, me.y);
        }

        public static Vector3Int ToVector3Int(this Vector3 me)
        {
            return new Vector3Int(me.x.FloatToInt(), me.y.FloatToInt(), me.z.FloatToInt());
        }

        public static bool IsInRange(this Vector3 me, Vector3 other, float radiusSq = 0.1f)
        {
            return (me - other).sqrMagnitude < radiusSq;
        }

        public static Vector3 DampToTarget(this Vector3 me, Vector3 target, float smoothing, float dt)
        {
            float factor = 1 - Mathf.Pow(smoothing, dt * 60f);

            return me.Lerp(target, factor);
        }

        public static Vector3 Lerp(this Vector3 me, Vector3 target, float progress)
        {
            return me + (target - me) * progress;
        }

        public static Color ToColour(this Vector3 me)
        {
            return new Color(me.x, me.y, me.z, 1f);
        }

        public static Vector3 Clamp(this Vector3 me, float mag)
        {
            float myMag = me.magnitude;
            if (myMag > mag)
            {
                return me * (mag / myMag);
            }

            return me;
        }

        //Assuming z is depth
        public static Quaternion ToQuaternionFromEuler(this Vector3 me)
        {
            return Quaternion.Euler(me);
        }

        /// <summary>
        /// Multiplies each component in this by its corresponding component in other
        /// </summary>
        /// <param name="me"></param>
        /// <param name="other"></param>
        /// <returns></returns>
        public static Vector3 Mult(this Vector3 me, Vector3 other)
        {
            return new Vector3(me.x * other.x, me.y * other.y, me.z * other.z);
        }

    }
}
