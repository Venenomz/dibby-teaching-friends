﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace LobsterDog
{
    public static class ExtensionsArray
    {
        public static void Swap<T>(this T[] array, int index0, int index1)
        {
            T temp = array[index0];
            array[index0] = array[index1];
            array[index1] = temp;
        }

        public static void Clear<T>(this T[] array)
        {
            Array.Clear(array, 0, array.Length);
        }

        public static void Swap<T>(this List<T> array, int index0, int index1)
        {
            T temp = array[index0];
            array[index0] = array[index1];
            array[index1] = temp;
        }

        public static T PickRandom<T>(this T[] array)
        {
            if (array.Length > 0)
            {
                // Unity Random Range INT is Max Exclusive.  Float is inclusive.
                return array[Random.Range(0, array.Length)];
            }

            return default(T);
        }

        public static T PickRandom<T>(this T[] array, int size)
        {
            if (array.Length > 0)
            {
                size = size.ClampToCeiling(array.Length);

                return array[Random.Range(0, size)];
            }

            return default(T);
        }

        public static int LastIndex<T>(this T[] array)
        {
            return array.Length - 1;
        }

        public static T LastElement<T>(this T[] array)
        {
            return array[array.Length - 1];
        }

        public static bool InRange(this System.Array array, int index0)
        {
            return index0 >= 0 && index0 < array.Length;
        }

        public static bool NotNullOrEmpty(this System.Array array)
        {
            return array.NotNull() && array.Length > 0;
        }

        public static bool IsNullOrEmpty(this System.Array array)
        {
            return array.IsNull() || array.Length <= 0;
        }

        public static int ClampRange(this System.Array array, int index)
        {
            if (index < 0)
                index = 0;
            else if (index >= array.Length)
                index = array.Length - 1;

            return index;
        }

        public static int WrapRange(this System.Array array, int index)
        {
            while (index < 0)
                index += array.Length;

            while (index >= array.Length)
                index -= array.Length;

            return index;
        }

        public static void Shuffle<T>(this T[] toShuffle)
        {

            for (int ii = 0; ii < toShuffle.Length / 2; ii++)
            {
                int swapIndex = Random.Range(ii, toShuffle.Length);

                T swap = toShuffle[swapIndex];
                toShuffle[swapIndex] = toShuffle[ii];
                toShuffle[ii] = swap;
            }
        }

        public static void ShuffleFront<T>(this T[] toShuffle, int sectionSizeFromFront)
        {
            Debug.Assert(sectionSizeFromFront <= toShuffle.Length, "ShuffleArraySection size needs to be <= array size");

            for (int ii = 0; ii < sectionSizeFromFront / 2; ii++)
            {
                int swapIndex = Random.Range(ii, sectionSizeFromFront);

                T swap = toShuffle[swapIndex];
                toShuffle[swapIndex] = toShuffle[ii];
                toShuffle[ii] = swap;
            }
        }

        /// <summary>
        /// Get a new array from a subset of this one as defined startIndex and length. ALLOCATES
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <param name="index"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static T[] SubArray<T>(this T[] data, int index, int length)
        {
            T[] result = new T[length];
            System.Array.Copy(data, index, result, 0, length);
            return result;
        }
    }

    public static class ExtensionsList
    {
        public static void Swap<T>(List<T> array, int index0, int index1)
        {
            T temp = array[index0];
            array[index0] = array[index1];
            array[index1] = temp;
        }

        public static bool NotNullOrEmpty<T>(this List<T> array)
        {
            return array.NotNull() && array.Count > 0;
        }

        public static bool IsNullOrEmpty<T>(this List<T> array)
        {
            return array.IsNull() || array.Count <= 0;
        }

        public static bool InRange<T>(this List<T> array, int index0)
        {
            return index0 >= 0 && index0 < array.Count;
        }

        public static T PickRandom<T>(this List<T> array)
        {
            return array[Random.Range(0, array.Count)];
        }

        public static T LastElement<T>(this List<T> array)
        {
            return array[array.Count - 1];
        }

        public static int LastIndex<T>(this List<T> array)
        {
            return array.Count - 1;
        }

        public static T FirstElement<T>(this List<T> array)
        {
            return array.Count > 0 ? array[0] : default(T);
        }

        public static int ClampRange<T>(this List<T> array, int index)
        {
            if (index < 0)
                index = 0;
            else if (index >= array.Count)
                index = array.Count - 1;

            return index;
        }

        public static void Shuffle<T>(this List<T> toShuffle)
        {
            for (int ii = 0; ii < toShuffle.Count / 2; ii++)
            {
                int swapIndex = Random.Range(ii, toShuffle.Count);

                T swap = toShuffle[swapIndex];
                toShuffle[swapIndex] = toShuffle[ii];
                toShuffle[ii] = swap;
            }
        }

        public static void ShiftDownIndices<T>(this List<T> toShuffle)
        {
            if (toShuffle.Count > 1)
            {
                T first = toShuffle[0];

                for (int ii = 0; ii < toShuffle.Count - 1; ii++)
                {
                    toShuffle[ii] = toShuffle[ii + 1];
                }

                toShuffle[toShuffle.Count - 1] = first;
            }
        }

        public static void ShiftDownIndices<T>(this T[] toShuffle)
        {
            if (toShuffle.Length > 1)
            {
                T first = toShuffle[0];

                for (int ii = 0; ii < toShuffle.Length - 1; ii++)
                {
                    toShuffle[ii] = toShuffle[ii + 1];
                }

                toShuffle[toShuffle.Length - 1] = first;
            }
        }

        public static void ShuffleFront<T>(this List<T> toShuffle, int sectionSizeFromFront)
        {
            Debug.Assert(sectionSizeFromFront <= toShuffle.Count, "ShuffleArraySection size needs to be <= array size");

            for (int ii = 0; ii < sectionSizeFromFront / 2; ii++)
            {
                int swapIndex = Random.Range(ii, sectionSizeFromFront);

                T swap = toShuffle[swapIndex];
                toShuffle[swapIndex] = toShuffle[ii];
                toShuffle[ii] = swap;
            }
        }
    }
}

