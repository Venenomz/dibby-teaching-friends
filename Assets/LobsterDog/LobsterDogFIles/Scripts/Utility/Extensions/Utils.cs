using UnityEngine;

namespace LobsterDog
{
    public static class Utils
    {
        #region Methods

        /// <summary>
        /// To remove having to use preprocessor directive. Much easier to read.
        /// </summary>
        /// <returns></returns>
        public static bool UnityEditor()
        {
#if UNITY_EDITOR
            return true;
#endif
            return false;
        }

        /// <summary>
        /// It's important to check for IOS when working on some projects as they have
        /// different line endings.
        /// </summary>
        /// <returns></returns>
        public static bool IOS()
        {
#if UNITY_IOS
            return true;
#endif
            return false;
        }

        public static bool Android()
        {
#if UNITY_ANDROID
            return true;
#endif
            return false;
        }

        #endregion
    }
}