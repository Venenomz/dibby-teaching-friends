﻿using UnityEngine;
using System.Collections;

namespace LobsterDog
{
	public static class ExtensionsBounds
	{

		/// <summary>
		/// Get the vector to move this bounds into other
		/// </summary>
		/// <param name="me"></param>
		/// <param name="other"></param>
		/// <returns></returns>
		public static Vector3 GetClampingVector(this Bounds me, Bounds other)
		{
			Vector3 result = Vector3.zero;

			Bounds grown = other;
			grown.Encapsulate(me);

			result.x = (grown.min.x - other.min.x) + (grown.max.x - other.max.x);
			result.y = (grown.min.y - other.min.y) + (grown.max.y - other.max.y);
			result.z = (grown.min.z - other.min.z) + (grown.max.z - other.max.z);

			return -result;
		}

		public static Vector3 GetClampingVector(this Bounds me, Vector3 other)
		{
			Vector3 result = Vector3.zero;

			if (other.x < me.min.x)
			{
				result.x = me.min.x - other.x;
			}
			else if (other.x > me.max.x)
			{
				result.x = me.max.x - other.x;
			}

			if (other.y < me.min.y)
			{
				result.y = me.min.y - other.y;
			}
			else if (other.y > me.max.y)
			{
				result.y = me.max.y - other.y;
			}

			if (other.z < me.min.z)
			{
				result.z = me.min.z - other.z;
			}
			else if (other.z > me.max.z)
			{
				result.z = me.max.z - other.z;
			}

			return result;
		}

		/// <summary>
		/// Same as bounds.Encapsulate, but works on nullables. Unlike Encapsulate?
		/// </summary>
		/// <param name="me"></param>
		/// <param name="other"></param>
		/// <returns></returns>
		public static Bounds Include(this Bounds me, Bounds other)
		{
			me.Encapsulate(other);
			return me;
		}

		//////////////////////////////////
		// Rect

		public static Rect Expand(this Rect me, Vector2 expansion)
		{
			me.x -= expansion.x;
			me.y -= expansion.y;
			me.width += expansion.x * 2;
			me.height += expansion.y * 2;

			if (me.width < 0)
			{
				//collapse
				me.x += me.width * 0.5f;
				me.width = 0;
			}

			if (me.height < 0)
			{
				//collapse
				me.y += me.height * 0.5f;
				me.height = 0;
			}

			return me;
		}

		public static Vector2 Constrain(this Rect me, Vector2 target)
		{
			if (target.x > me.xMax)
			{
				target.x = me.xMax;
			}
			else if (target.x < me.xMin)
			{
				target.x = me.xMin;
			}

			if (target.y > me.yMax)
			{
				target.y = me.yMax;
			}
			else if (target.y < me.yMin)
			{
				target.y = me.yMin;
			}

			return target;
		}
	}
}