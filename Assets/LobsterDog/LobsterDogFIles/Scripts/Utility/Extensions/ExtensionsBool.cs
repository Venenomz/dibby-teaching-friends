﻿using UnityEngine;
using System.Collections;

namespace LobsterDog
{
	public static class ExtensionsBool
	{
		public static float ToPlusMinusFloat(this bool me)
		{
			return me ? 1.0f : -1.0f;
		}

		public static int ToPlusMinusInt(this bool me)
		{
			return me ? 1 : -1;
		}
	}
}
