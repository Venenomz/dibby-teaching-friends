﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace LobsterDog
{
	public static class ExtensionsInt
	{
		static readonly Dictionary<int, string> s_numberStrings = new Dictionary<int, string>();

		public static string ToStringCached(this int me)
		{
			string outString = null;
			if (s_numberStrings.TryGetValue(me, out outString))
			{
				return outString;
			}
			else
			{
				string num = me.ToString();
				s_numberStrings[me] = num;

				return num;
			}
		}

		public static int Lerp(this int me, int target, float prog)
		{
			return (int)(me + (target - me) * prog);
		}

		public static void Clamp(ref int me, int min, int max)
		{
			if (me > max)
			{
				me = max;
			}
			else if (me < min)
			{
				me = min;
			}
		}

		public static int Clamp(this int me, int min, int max)
		{
			Clamp(ref me, min, max);
			return me;
		}


		//Wrap between min and max (non inclusive of max)
		public static int Wrap(this int me, int min, int max)
		{
			if (min == max)
			{
				return min;
			}
			else if (max < min)
			{
				int t = min;
				min = max;
				max = t;
			}

			int diff = max - min;
			while (me < min)
			{
				me += (diff);
			}
			while (me >= max)
			{
				me -= (diff);
			}
			return me;
		}

		public static int ClampToCeiling(this int me, int ceiling)
		{
			if (me > ceiling)
			{
				return ceiling;
			}

			return me;
		}

		public static int ClampToFloor(this int me, int floor)
		{
			if (me < floor)
			{
				return floor;
			}
			return me;
		}

		public static int Abs(this int me)
		{
			return Mathf.Abs(me);
		}

		public static int Sign(this int me)
		{
			return me >= 0 ? 1 : -1;
		}

		public static bool IsOdd(this int me)
		{

			return (me & 1) > 0;
		}

		public static bool IsEven(this int me)
		{

			return (me & 1) == 0;
		}

		public static int RoundToClosestMultipleOf(this int me, int multiple)
		{
			return Mathf.RoundToInt((float)me / multiple) * multiple;
		}

		public static bool PercentChance(this int me)
		{
			return Random.value < (float)me / 100;
		}

		/// <summary>
		/// Add a random amount
		/// </summary>
		/// <param name="me"></param>
		/// <param name="range"></param>
		/// <returns></returns>
		public static int PlusRandomAmount(this int me, int range)
		{
			return me + Random.Range(0, range);
		}

		public static int RandomSign(this int me)
		{
			return me * (Random.value < 0.5f ? 1 : -1);
		}

		/// <summary>
		/// Add a set amount with a given chance 0 - 1
		/// </summary>
		/// <param name="me"></param>
		/// <param name="range"></param>
		/// <returns></returns>
		public static int PlusSetAmountWithChance(this int me, int amount, float chance)
		{
			return me + (Random.value < chance ? amount : 0);
		}

		public static float Map(this int me, int inputMin, int inputMax, float outPutMin, float outputMax)
		{
			return outPutMin + (((float)me).NormaliseFromRange(inputMin, inputMax) * (outputMax - outPutMin));
		}

		public static float Map(this int me, int inputMin, int inputMax, float outPutMin, float outputMax, System.Func<float, float, float, float> easeFunc)
		{
			return easeFunc(outPutMin, outputMax, ((float)me).NormaliseFromRange(inputMin, inputMax));
		}
	}
}

