using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace LobsterDog
{
    [System.Serializable]
    public struct float01
    {
        [SerializeField] float _value;
        public float Value
        {
            get
            {
                return _value;
            }
            set
            {
                //Speed tested, found this way was 10% faster than Mathf.Clamp01
                if (value < 0)
                {
                    value = 0;
                }
                else if (value > 1)
                {
                    value = 1;
                }

                _value = value;
            }
        }

        public float01(float a_value)
        {
            _value = Mathf.Clamp01(a_value);
        }

        public float01(float a_low, float a_high, float a_current)
        {
            _value = Mathf.Clamp01((a_current - a_low) / (a_high - a_low));
        }

        public static float operator *(float01 a_obj1, int a_obj2)
        {
            return a_obj1.Value * a_obj2;
        }

        public static float operator *(float01 a_obj1, float a_obj2)
        {
            return a_obj1.Value * a_obj2;
        }

        public static float operator *(float a_obj1, float01 a_obj2)
        {
            return a_obj1 * a_obj2.Value;
        }

        public static float operator -(float01 a_obj1, float a_obj2)
        {
            return a_obj1.Value - a_obj2;
        }

        public static float operator -(float a_obj2, float01 a_obj1)
        {
            return a_obj2 - a_obj1.Value;
        }

        public static float operator +(float01 a_obj1, float a_obj2)
        {
            return a_obj1.Value + a_obj2;
        }

        public static float operator +(float a_obj2, float01 a_obj1)
        {
            return a_obj1.Value + a_obj2;
        }

        public static bool operator ==(float01 a_obj1, float a_obj2)
        {
            return (a_obj1.Value == a_obj2);
        }

        public static bool operator !=(float01 a_obj1, float a_obj2)
        {
            return (a_obj1.Value != a_obj2);
        }

        public static bool operator ==(float01 a_obj1, float01 a_obj2)
        {
            return (a_obj1.Value == a_obj2.Value);
        }

        public static bool operator !=(float01 a_obj1, float01 a_obj2)
        {
            return (a_obj1.Value != a_obj2.Value);
        }

        public static implicit operator float01(float a_x)
        {
            return new float01(a_x);
        }

        public override bool Equals(object a_obj)
        {
            if (a_obj == null || !(a_obj is float01))
            {
                return false;
            }

            float01 obj01 = (float01)a_obj;

            return (obj01 == this);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
    }
}