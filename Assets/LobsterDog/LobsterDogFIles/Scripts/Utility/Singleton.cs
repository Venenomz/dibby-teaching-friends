using UnityEngine;

namespace LobsterDog
{
    [DisallowMultipleComponent]
    public abstract class Singleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        static T _instance;
        public static T Instance
        {
            get
            {
                if (!_instance)
                { _instance = FindObjectOfType<T>(); }
                return _instance;
            }
            private set { _instance = value; }
        }
    

        protected virtual void Awake()
        {
            SetInstance();
        }

        void SetInstance()
        {
            if (Instance && Instance != this)
            {
                if (Utils.UnityEditor())
                    DebugTools.Log("An instance already exists: " + Instance.name);

                Destroy(this);
            }
            Instance = this as T;
        }
    }
}